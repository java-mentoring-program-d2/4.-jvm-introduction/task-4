package classloaders;

import model.Animal;
import model.Cat;
import model.Dog;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class MyClassLoader extends ClassLoader {
    public static void main(String[] args) {
        try {
            MyClassLoader myClassLoader = new MyClassLoader();
            Class<?> catClass = myClassLoader.loadClass("model.Cat");
            Class<?> dogClass = myClassLoader.loadClass("model.Dog");

            List<Animal> animals = new ArrayList<>();
            animals.add((Cat) catClass.getConstructor().newInstance());
            animals.add((Dog) dogClass.getConstructor().newInstance());

            animals.forEach(Animal::play);
            animals.forEach(Animal::voice);

        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InstantiationException
                | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        System.out.println("Loading Class '" + name + "'");
        return super.loadClass(name);
    }
}
