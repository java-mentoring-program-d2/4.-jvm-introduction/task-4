package model;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public interface Animal {
    Logger logger = LogManager.getRootLogger();
    void play();
    void voice();
}
