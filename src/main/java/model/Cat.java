package model;

public class Cat implements Animal {
    @Override
    public void play() {
        logger.debug("model.Cat is playing");
    }

    @Override
    public void voice() {
        logger.debug("model.Cat is meowing");
    }
}
