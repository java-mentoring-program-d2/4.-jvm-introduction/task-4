package model;

public class Dog implements Animal {
    @Override
    public void play() {
        logger.debug("model.Dog is playing");
    }

    @Override
    public void voice() {
        logger.debug("model.Dog is barking");
    }
}
